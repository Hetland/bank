﻿# Bank

.Net Core Web app (with React front)

A website with users and fake bank accounts, where you can do transactions and view your different bank accounts


## Getting started
With the .NET Core SDK installed, go into the folder and run
~~~
dotnet new sln --name Bank
dotnet sln Bank.sln add ./Bank.csproj
~~~

### To run it locally
Open the .sln file in Visual Studio
In appsettings.json, change "DefaultConnection" to "Server=(localdb)\\mssqllocaldb;Database=aspnet-simplebank-416C6FD2-3531-42D6-9EDE-18AC45901208;Trusted_Connection=True;MultipleActiveResultSets=true"
Go to the package manager console, and type in
~~~
Update-Database
~~~
Then run the following inside the ClientApp folder
~~~
npm i
~~~
Now run the code by choosing IIS Express


### To push from Gitlab and run on Azure
First create a database on Azure, as well as a container registry
Get the connection string from the database, and replace DefaultConnection in appsettings.json with the connection string
Ensure that the password to the database is included in the connection string
Go to the package manager console, and type in
~~~
Update-Database
~~~
Next, make the Gitlab CI/CD variables AZURE_LOGIN_PORTAL, AZURE_USERNAME and AZURE_PWD using the url (f.ex. simplebankapp.azurecr.io), admin username and pwd for the container registry
Push the solution up to your project folder on Gitlab
Log into Azure, and create a container instance with the image in the container registry



## Prerequisites
* .NET Core SDK
* React
* Visual Studio 2017/19 OR Visual Studio Code
* Docker


## Authors
* Jens CT Hetland |   [Hetland](https://gitlab.com/Hetland)