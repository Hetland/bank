﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Bank.Data;
using Bank.Models;
using Microsoft.AspNetCore.Authorization;

namespace Bank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TransactionController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public TransactionController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Transaction
        [HttpGet("allForSpecificHistory/{historyId}")]
        public async Task<ActionResult<IEnumerable<Transaction>>> GetTransactions(int historyId)
        {
            return await _context.Transactions.Where(t => t.HistoryId == historyId).OrderByDescending(t => t.TimeOfTransaction).ToListAsync();
        }

        // GET: api/Transaction/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Transaction>> GetTransaction(int id)
        {
            var transaction = await _context.Transactions.FindAsync(id);

            if (transaction == null)
            {
                return NotFound();
            }

            return transaction;
        }

        // PUT: api/Transaction/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTransaction(int id, Transaction transaction)
        {
            if (id != transaction.TransactionId)
            {
                return BadRequest();
            }

            _context.Entry(transaction).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Transaction
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Transaction>> PostTransaction(Transaction transaction)
        {
            _context.Transactions.Add(transaction);
            var accountToSendFrom = await _context.BankAccounts.Where(ba => ba.History.HistoryId == transaction.HistoryId).FirstAsync();
            accountToSendFrom.MoneyInAccount -= transaction.Amount;
            await _context.SaveChangesAsync();

            var accountsMatchingOutgoing = _context.BankAccounts.Where(ba => ba.InternationalAccountIdentifier == transaction.INTLAccountNumber);
            if (accountsMatchingOutgoing.Any())
            {
                var accountToSendTo = await accountsMatchingOutgoing.Include(ba => ba.History).FirstAsync();
                Transaction incomingTransaction = new Transaction { 
                    Incoming = true,
                    Amount = transaction.Amount,
                    TimeOfTransaction = transaction.TimeOfTransaction,
                    INTLAccountNumber = transaction.INTLAccountNumber,
                    HistoryId = accountToSendTo.History.HistoryId
                };
                accountToSendTo.MoneyInAccount += transaction.Amount;
                _context.Transactions.Add(incomingTransaction);
            }
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTransaction", new { id = transaction.TransactionId }, transaction);
        }

        // DELETE: api/Transaction/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Transaction>> DeleteTransaction(int id)
        {
            var transaction = await _context.Transactions.FindAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }

            _context.Transactions.Remove(transaction);
            await _context.SaveChangesAsync();

            return transaction;
        }

        private bool TransactionExists(int id)
        {
            return _context.Transactions.Any(e => e.TransactionId == id);
        }
    }
}
