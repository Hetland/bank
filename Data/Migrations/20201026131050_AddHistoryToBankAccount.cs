﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bank.Data.Migrations
{
    public partial class AddHistoryToBankAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Histories_BankAccountId",
                table: "Histories");

            migrationBuilder.CreateIndex(
                name: "IX_Histories_BankAccountId",
                table: "Histories",
                column: "BankAccountId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Histories_BankAccountId",
                table: "Histories");

            migrationBuilder.CreateIndex(
                name: "IX_Histories_BankAccountId",
                table: "Histories",
                column: "BankAccountId");
        }
    }
}
