﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bank.Models
{
    public class History
    {
        [Key]
        public int HistoryId { get; set; }
        [ForeignKey("BankAccount")]
        public int BankAccountId { get; set; }
        public BankAccount BankAccount { get; set; }
        public IEnumerable<Transaction> Transactions { get; set; }
    }
}
