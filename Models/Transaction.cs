﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bank.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }
        [Required]
        public bool Incoming { get; set; }
        [Required]
        public float Amount { get; set; }
        [Required]
        public DateTime TimeOfTransaction { get; set; }
        [Required]
        public int INTLAccountNumber { get; set; }

        public int HistoryId { get; set; }
        public History History { get; set; }
    }
}
