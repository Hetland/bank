﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bank.Models
{
    public class BankAccount
    {
        [Key]
        public int BankAccountId { get; set; }
        [Range(100000000, 999999999)]   // (1 / 10^9) chance of getting the same number (good enough for demonstration, but not for a release)
        public int InternationalAccountIdentifier { get; set; }
        public float MoneyInAccount { get; set; }

        [ForeignKey("Profile")]
        [Required]
        public string ProfileId { get; set; }
        public Profile Profile { get; set; }

        [ForeignKey("AccountType")]
        public int? AccountTypeId { get; set; }
        public AccountType? AccountType { get; set; }

        public History History { get; set; }
    }
}
