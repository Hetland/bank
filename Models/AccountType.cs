﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bank.Models
{
    public class AccountType
    {
        [Key]
        public int AccountTypeId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public float Rent { get; set; }
        public IEnumerable<BankAccount> BankAccounts { get; set; }
    }
}
