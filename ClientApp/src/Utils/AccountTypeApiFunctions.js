﻿import * as axios from 'axios';
import authService from '../components/api-authorization/AuthorizeService';

export async function getAccountTypes() {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/accountType', {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data; //.map(g => {
//        return { ...g };
//    });
}

export async function getAccountTypeSpecificBankAccount(bankAccountId) {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/accountType/forSpecificAccount/' + bankAccountId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}


export async function getAccountType(accountTypeId) {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/accountType/' + accountTypeId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function joinAccountType(accountTypeId, bankAccountId) {
    const token = await authService.getAccessToken();
    let response = await axios.post(`api/accountType/${accountTypeId}/join`, bankAccountId || [], {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function leaveAccountType(accountTypeId, bankAccountId) {
    const token = await authService.getAccessToken();
    const response = await axios.delete('api/accountType/' + accountTypeId + '/leave', bankAccountId || [] , {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function postAccountType(accountType) {
    const token = await authService.getAccessToken();
    const response = await axios.post('api/accountType', accountType, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function putAccountType(accountType) {
    const token = await authService.getAccessToken();
    const response = await axios.put('api/accountType', accountType, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function deleteAccountType(accountTypeId) {
    const token = await authService.getAccessToken();
    const response = await axios.delete('api/accountType', accountTypeId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}
