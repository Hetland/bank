﻿import * as axios from 'axios';
import authService from '../components/api-authorization/AuthorizeService';

export async function getTransactions(historyId) {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/transaction/allForSpecificHistory/' + historyId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data; //.map(g => {
//        return { ...g };
//    });
}

export async function getTransaction(transactionId) {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/transaction/' + transactionId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function postTransaction(transaction) {
    const token = await authService.getAccessToken();
    const response = await axios.post('api/transaction', transaction, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function putTransaction(transaction) {
    const token = await authService.getAccessToken();
    const response = await axios.put('api/transaction', transaction, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function deleteTransaction(transactionId) {
    const token = await authService.getAccessToken();
    const response = await axios.delete('api/transaction', transactionId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}
