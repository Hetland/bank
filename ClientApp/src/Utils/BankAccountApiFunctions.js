﻿import * as axios from 'axios';
import authService from '../components/api-authorization/AuthorizeService';
import { getUserId } from './HelperFunctions';

export async function getBankAccounts() {
    const token = await authService.getAccessToken();
    const userId = await getUserId();
    const response = await axios.get('api/bankAccount/allForSpecificUser/' + userId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function getBankAccount(bankAccountId) {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/bankAccount/' + bankAccountId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function postBankAccount(bankAccount) {
    const token = await authService.getAccessToken();
    const response = await axios.post('api/bankAccount', bankAccount, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function putBankAccount(bankAccount) {
    const token = await authService.getAccessToken();
    const response = await axios.put('api/bankAccount', bankAccount, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function deleteBankAccount(bankAccountId) {
    const token = await authService.getAccessToken();
    const response = await axios.delete('api/bankAccount', bankAccountId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function seedBankAccounts() {
    const token = await authService.getAccessToken();
    const userId = await getUserId();
    const response = await axios.post('api/bankAccount/seed/' + userId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}
