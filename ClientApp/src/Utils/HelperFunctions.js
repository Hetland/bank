﻿import * as _ from 'lodash';
import authService from '../components/api-authorization/AuthorizeService';
import { getBankAccounts, seedBankAccounts } from './BankAccountApiFunctions';
import { getTransactions } from './TransactionApiFunctions';
import { getHistory } from './HistoryApiFunctions';
import { getAccountType } from './AccountTypeApiFunctions';

// helper functions for reducers

export function concatItemsWithId(oldItems, newItems) {
    const newList = newItems.concat(oldItems);
    return _.uniqBy(newList, i => i.id);
}

export function concatProfiles(oldProfiles, newProfiles) {
    const newList = [].concat(newProfiles).concat(oldProfiles);
    return _.uniqBy(newList, i => i.userId);
}

export function stringifyDateTime(time) {
    const dateTime = new Date(time).toDateString().split(' ');
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const day = days[new Date(time).getDay()]
    const clock = new Date(time).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    return `${day}, ${dateTime[2]} ${dateTime[1]} ${dateTime[3]} at ${clock}`;
}

export async function getUserId() {
    return (await authService.getUser()).sub;
}

export async function getOrCreateAllBankAccounts() {
    var bas = await getBankAccounts();
    if (bas.length == 0) {
        seedBankAccounts();
        bas = await getBankAccounts();
    }
    for (var bankAccountNum = 0; bankAccountNum < bas.length; bankAccountNum++) {
        var typeForAccount = await getAccountType(bas[bankAccountNum].bankAccountId);
        var historyForAccount = await getHistory(bas[bankAccountNum].bankAccountId);
        var transactionsForAccount = await getTransactions(historyForAccount.historyId);
        historyForAccount.transactions = transactionsForAccount;
        bas[bankAccountNum].history = historyForAccount;
        bas[bankAccountNum].accountType = typeForAccount;
    }
    return bas;
}