﻿import * as axios from 'axios';
import authService from '../components/api-authorization/AuthorizeService';

export async function getProfile(id) {
    const token = await authService.getAccessToken();
    const response = await axios.get(`api/profile/${id}`, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function postProfile(userId, profile) {
    const token = await authService.getAccessToken();
    const response = await axios.post('api/profile', profile, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function putProfile(id, profile) {
    const token = await authService.getAccessToken();
    const response = await axios.put(`api/profile/${id}`, profile, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function getProfiles() {
    const token = await authService.getAccessToken();
    const response = await axios.get(`api/profile`, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}

export async function deleteProfile(id) {
    const token = await authService.getAccessToken();
    const response = await axios.delete('api/profile', id, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data;
}