﻿import * as axios from 'axios';
import authService from '../components/api-authorization/AuthorizeService';

export async function getHistorys() {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/history', {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return response.data; //.map(g => {
//        return { ...g };
//    });
}

export async function getHistory(bankAccountId) {
    const token = await authService.getAccessToken();
    const response = await axios.get('api/history/' + bankAccountId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function joinHistory(historyId, bankAccountId) {
    const token = await authService.getAccessToken();
    let response = await axios.post(`api/history/${historyId}/join`, bankAccountId || [], {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function leaveHistory(historyId, bankAccountId) {
    const token = await authService.getAccessToken();
    const response = await axios.delete('api/history/' + historyId + '/leave', bankAccountId || [] , {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function postHistory(history) {
    const token = await authService.getAccessToken();
    const response = await axios.post('api/history', history, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function putHistory(history) {
    const token = await authService.getAccessToken();
    const response = await axios.put('api/history', history, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}

export async function deleteHistory(historyId) {
    const token = await authService.getAccessToken();
    const response = await axios.delete('api/history', historyId, {
        headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    return { ...response.data };
}
