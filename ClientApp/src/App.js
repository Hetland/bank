import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';

import { Home } from './components/Home';
import { AccountsOverview } from './components/Accounts/AccountsOverview';
import { Account } from './components/Accounts/Account';
import { AccountTypesOverview } from './components/AccountTypes/AccountTypesOverview';
import { Profile } from './components/Profile/Profile';


import AuthorizeRoute from './components/api-authorization/AuthorizeRoute';
import ApiAuthorizationRoutes from './components/api-authorization/ApiAuthorizationRoutes';
import { ApplicationPaths } from './components/api-authorization/ApiAuthorizationConstants';

import './custom.css'
import { MakeTransaction } from './components/Transactions/MakeTransaction';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route exact path='/accounts' component={AccountsOverview} />
        <Route exact path='/account/:id' component={Account} />
        <Route exact path='/maketransactions' component={MakeTransaction} />
        <Route exact path='/accountTypes' component={AccountTypesOverview} />
        <Route exact path='/profile' component={Profile} />
        <Route path={ApplicationPaths.ApiAuthorizationPrefix} component={ApiAuthorizationRoutes} />
      </Layout>
    );
  }
}
