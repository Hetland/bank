import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as renderIf from 'render-if';

export class AccountTypesOverview extends Component {
    static displayName = AccountTypesOverview.name;

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
        }
    }

    async getAccountTypes() {
        throw new Error("Fetching account types not implemented!");
    }

    componentDidMount() {
        this.getAccountTypes();
    }

    render() {
        return (
            <div>
                {renderIf(this.state.isLoading)(() => (
                    <div> Loading account types... </div>
                ))}
                {renderIf(!this.state.isLoading)(() => (
                    <div>
                        {renderIf(this.state.error)(() => (
                            <div>
                                There was an error getting the account types, please reload the page. If the problem persists,
                                don't hesitate to contact us!
                                <br />
                                {this.state.error.message}
                            </div>
                        ))}
                        {renderIf(this.state.accountTypes)(() => (
                            <div>
                                Here be accountTypes, matey!
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        );
    }
}
