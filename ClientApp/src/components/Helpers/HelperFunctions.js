﻿import authService from "../api-authorization/AuthorizeService";

export async function getProfileId() {
    return (await authService.getUser).sub;
}


