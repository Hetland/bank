import React, { Component } from 'react';
import { getProfile } from '../../Utils/ProfileApiFunctions';
import { getUserId } from '../../Utils/HelperFunctions';

import * as renderIf from 'render-if';
import { Card, CardBody, CardText, CardTitle } from 'reactstrap';
import AlertDialog from './ChangeProfile';

export class Profile extends Component {
    static displayName = Profile.name;

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
        };

        this.giveEmail = this.giveEmail.bind(this);
        this.givePhone = this.givePhone.bind(this);
        this.handleUpdatedProfile = this.handleUpdatedProfile.bind(this);
    };

    async getProfile() {
        const id = await getUserId();
        const profile = await getProfile(id);
        this.setState({ profile, isLoading: false });
    }

    giveEmail() {
        return this.state.profile.email;
    };

    givePhone() {
        return (this.state.profile.phoneNumber ? this.state.profile.phoneNumber : "12345678");
    };

    handleUpdatedProfile() {
        window.location.reload(true);
    };

    componentDidMount() {
        this.getProfile();
    }

    render() {
        const profileCardStyle = {
            width: "18em",
            marginTop: "100px",
            marginLeft: "auto",
            marginRight: "auto",
            
        }
        const profile = this.state.profile;
        return (
            <div>
                {renderIf(profile)(() => (
                    <div>
                        <Card style={profileCardStyle}>
                            {renderIf(this.state.isLoading)(() => (
                                <CardBody>
                                    <CardTitle>
                                        Loading details...
                                    </CardTitle>
                                </CardBody>
                            ))}
                            {renderIf(profile)(() => (
                                <React.Fragment>
                                <CardBody>
                                        <CardTitle style={{ fontSize: "200%" }}>
                                        {profile.firstName + " " + profile.lastName}
                                    </CardTitle>
                                </CardBody>
                                <CardBody>
                                    <CardTitle>
                                            <AlertDialog getEmail={this.giveEmail} profile={this.state.profile}/>
                                    </CardTitle>
                                </CardBody>
                                    <CardBody>
                                        <CardTitle>
                                            <AlertDialog getPhone={this.givePhone} profile={this.state.profile} />
                                        </CardTitle>
                                    </CardBody>
                                </React.Fragment>
                            ))}
                        </Card>
                    </div>
                ))}
            </div>
        );
    }
}
