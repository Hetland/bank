﻿import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import * as renderIf from 'render-if';
import { putProfile } from '../../Utils/ProfileApiFunctions';

export default function FormDialog(props) {
    const [open, setOpen] = React.useState(false);
    const [itemToUpdate, setItemToUpdate] = React.useState("");
    const title = props.getEmail ? "Change your email" : "Change your phone number";
    const typeToChange = props.getEmail ? "email" : "phone number";


    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleUpdate = async () => {
        setOpen(false);
        var updatedProfile = props.profile;
        if (props.getEmail) {
            updatedProfile.email = itemToUpdate;
        } else {
            updatedProfile.phoneNumber = itemToUpdate;
        }
        try {
            await putProfile(updatedProfile.userId, updatedProfile);
            window.location.reload();
        } catch (e) {
            console.log(e);
        }

    };


    return (
        <div>
            <Button color="primary" onClick={handleClickOpen}>
                {renderIf(props.getEmail)(() => (
                    props.getEmail()
                ))}
                {renderIf(props.getPhone)(() => (
                    props.getPhone()
                ))}
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        PLease enter your new {typeToChange} below
                </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id={"textfield" + typeToChange.replace(" ", "")}
                        label={"New " + typeToChange}
                        type={typeToChange}
                        onChange={(event) => { setItemToUpdate(event.target.value); }}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleUpdate} color="primary">
                        Change
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}