﻿import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import { Card, CardContent, Select, CardActions, Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    addMarginsBottom: {
        marginBottom: '20px'
    },
    addMarginsButton: {
        marginTop: '50px'
    },
    accountToSendFrom: {
        borderRadius: 4,
        border: '1px solid #ced4da',
        fontSize: 16,
        padding: '10px 26px 10px 12px',
        marginBottom: '20px'
    },
    transactionCardStyle: {
        padding: '50px'
    }
}));

export default function TransactionForm(props) {
    const classes = useStyles();
    const [sent, setSent] = React.useState(false);
    const [sending, setSending] = React.useState(false);
    const [state, setState] = React.useState({
        accountToSendFrom: props.bankAccounts[0].bankAccountId,
        accountToSendTo: "",
        amountToSend: ""
    });

    const handleAccountToSendFromChange = (event) => {
        setState({
            ...state,
            accountToSendFrom: event.target.value
        });
    };

    const handleChange = (event) => {
        const name = event.target.name;
        setState({
            ...state,
            [name]: event.target.value,
        });
    };

    return (
        <Card className={classes.transactionCardStyle}>
            <CardContent>
                <FormControl className={classes.formControl}>
                    <InputLabel shrink htmlFor="account-native-label-placeholder">
                        Account to send from
                    </InputLabel>
                    <NativeSelect
                        value={state.accountToSendFrom}
                        onChange={handleChange}
                        className={classes.accountToSendFrom}
                        inputProps={{
                            name: 'accountToSendFrom',
                            id: 'age-native-simple',
                        }}
                    >
                        {props.bankAccounts.map((ba) =>
                            <option key={"option" + ba.accountType.name} value={ba.bankAccountId}>{ba.accountType.name}</option>
                        )}
                    </NativeSelect>
                    <TextField variant="outlined" className={classes.addMarginsBottom} name="accountToSendTo" label="Account to send to" value={state.accountToSendTo} onChange={handleChange} />
                    <TextField variant="outlined" className={classes.addMarginsBottom} name="amountToSend" label="Amount" value={state.amountToSend} onChange={handleChange} />
                    <Button size="large" className={classes.addMarginsButton} onClick={() => { props.sendTransactionDetails(state) }}>
                        Send
                    </Button>
                </FormControl>
            </CardContent>
        </Card>
    );
}