import React, { Component } from 'react';
import * as renderIf from 'render-if';
import TransactionForm from './TransactionForm';
import { getOrCreateAllBankAccounts } from '../../Utils/HelperFunctions';
import { postTransaction } from '../../Utils/TransactionApiFunctions';

export class MakeTransaction extends Component {
    static displayName = MakeTransaction.name;

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
        };

        this.sendTransactionDetails = this.sendTransactionDetails.bind(this);
    };

    async getAllBankAccounts() {
        const bas = await getOrCreateAllBankAccounts();
        this.setState({ bankAccounts: bas, isLoading: false });
    }

    componentDidMount() {
        this.getAllBankAccounts();
    }

    async sendTransactionDetails(thingsToSend) {
        if (thingsToSend.accountToSendFrom && thingsToSend.accountToSendTo && thingsToSend.amountToSend) {
            const transaction = {
                incoming: false,
                amount: thingsToSend.amountToSend,
                timeOfTransaction: new Date(),
                iNTLAccountNumber: thingsToSend.accountToSendTo,
                historyId: this.state.bankAccounts
                    .find(ba => ba.bankAccountId == thingsToSend.accountToSendFrom)
                    .history.historyId
            };
            await postTransaction(transaction);
            window.history.go();
        //this.setState({ transactionSent: true });
        }
    }

    render() {
        const transactionFormStyle = { display: "flex", minHeight: "400px", justifyContent: "center", alignItems: "center" };

        return (
            <div>
                {renderIf(this.state.isLoading)(() => (
                    <div></div>
                ))}
                {renderIf(!this.state.isLoading)(() => (
                    <div>
                        {renderIf(this.state.error)(() => (
                            <div>
                                There was an error loading the page, please reload. If the problem persists,
                                don't hesitate to contact us!
                                <br />
                                {this.state.error.message}
                            </div>
                        ))}
                        {renderIf(this.state.bankAccounts && !(this.state.transactionSent))(() => (
                            <div style={transactionFormStyle}>
                                <TransactionForm bankAccounts={this.state.bankAccounts} sendTransactionDetails={this.sendTransactionDetails}/>
                            </div>
                        ))}
                        {renderIf(this.state.transactionSent)(() => (
                            <div>

                            </div>
                        ))}
                    </div>
                ))}
            </div>
        );
    }
}
