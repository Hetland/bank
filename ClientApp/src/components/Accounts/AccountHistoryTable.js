﻿import React from 'react';
import * as moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    table: {
        minWidth: 250,
    },
});

export default function AccountHistoryTable(props) {
    const classes = useStyles();
    const alignment = "inherit";
    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} size="small" aria-label="a dense table">
                <TableHead>
                    <TableRow>
                        <TableCell align={alignment}>Amount</TableCell>
                        <TableCell align={alignment}>Time of transaction</TableCell>
                        <TableCell align={alignment}>To</TableCell>
                        <TableCell align={alignment}>From</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.transactions.map((t) => (
                        <React.Fragment key={t.transactionId}>
                            <TableRow key={"row" + t.transactionId}>
                                <TableCell key={"amount" + t.transactionId} align={alignment}>{t.incoming ? "  " : "- "}{t.amount}</TableCell>
                                <TableCell key={"date" + t.transactionId} align={alignment}>{moment.utc(t.timeOfTransaction).local().fromNow()}</TableCell>
                                <TableCell key={"outgoing" + t.transactionId} align={alignment}>{t.incoming ? "" : t.intlAccountNumber}</TableCell>
                                <TableCell key={"incoming" + t.transactionId} align={alignment}>{t.incoming ? t.intlAccountNumber : ""}</TableCell>
                            </TableRow>
                        </React.Fragment>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}