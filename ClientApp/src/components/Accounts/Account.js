import React, { Component } from 'react';
import * as renderIf from 'render-if';
import { getBankAccount } from '../../Utils/BankAccountApiFunctions';

export class Account extends Component {
    static displayName = Account.name;

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
        };
    };

    async getBankAccount() {
        const url = window.location.href;
        const splitUrl = url.split('/');
        const id = parseInt( splitUrl[ splitUrl.length - 1 ] )   // Gets the last item, which is the id in this case, and tries to convert it into a number
        const ba = await getBankAccount(id);
        this.setState({ bankAccount: ba });
    }

    componentDidMount() {

        this.getBankAccount();
    }

    render() {
        return (
            <div>
                {renderIf(this.state.isLoading)(() => (
                    <div> Loading account... </div>
                ))}
                {renderIf(!this.state.isLoading)(() => (
                    <div>
                        {renderIf(this.state.error)(() => (
                            <div>
                                There was an error getting the account, please reload the page. If the problem persists,
                                don't hesitate to contact us!
                                <br />
                                {this.state.error.message}
                            </div>
                        ))}
                        {renderIf(this.state.bankAccount)(() => (
                            <div>
                                Info on a bank account
                                {this.state.bankAccount.id}
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        );
    }
}
