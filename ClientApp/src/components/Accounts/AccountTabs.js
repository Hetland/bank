﻿import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import AccountHistoryTable from './AccountHistoryTable';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

function getAllTransactions(allTransactions, bankAccount) {
    return [...allTransactions, ...bankAccount.history.transactions];
}

const sortDates = (a, b) => {
    return new Date(b.timeOfTransaction) - new Date(a.timeOfTransaction);
};

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

export default function AccountTabs(props) {
    const classes = useStyles();
    const commonAccountStyle = { marginTop: "20px", marginBottom: "50px", fontSize: "large", fontWeight: "bold" };
    const moneyInAccountStyleGood = { ...commonAccountStyle, color: "green"};
    const moneyInAccountStyleBad = { ...commonAccountStyle, color: "red" };
    const intlAccountIdentifierStyle = {marginTop: "20px", marginBottom: "50px"};
    const [value, setValue] = React.useState("All");
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
                    <Tab label="All" {...a11yProps("All")} value={"All"} />
                    {props.bankAccounts.map((ba) =>
                        <Tab key={"tab" + ba.accountType.name} label={ba.accountType.name} value={ba.accountType.name} {...a11yProps(ba.accountType.name)} /> 
                    )}
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={"All"}>
                <AccountHistoryTable key={"tableForAllTransactions"} transactions={props.bankAccounts.reduce(getAllTransactions, []).sort(sortDates)} />
            </TabPanel>
            {props.bankAccounts.map((ba) =>
                <TabPanel key={"panel" + ba.accountType.name} value={value} index={ba.accountType.name}>
                    <div key={"moneyInAccount" + ba.accountType.name} style={ (ba.moneyInAccount >= 0.0) ? moneyInAccountStyleGood : moneyInAccountStyleBad}>
                        {ba.moneyInAccount} 
                    </div>
                    <div key={"intlaccountnumber" + ba.accountType.name} style={intlAccountIdentifierStyle}>
                        International Account Identifier: {ba.internationalAccountIdentifier}
                    </div>
                    <AccountHistoryTable key={"table" + ba.accountType.name} transactions={ba.history.transactions} />
                </TabPanel>
            )}
        </div>
    );
}