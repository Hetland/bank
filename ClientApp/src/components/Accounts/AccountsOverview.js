import React, { Component } from 'react';
import * as renderIf from 'render-if';
import { Link } from 'react-router-dom';
import AccountTabs from './AccountTabs';
import { getUserId, getOrCreateAllBankAccounts } from '../../Utils/HelperFunctions';
export class AccountsOverview extends Component {
    static displayName = AccountsOverview.name;

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
        };
    };

    async getAllBankAccounts() {
        const bas = await getOrCreateAllBankAccounts();
        this.setState({ bankAccounts: bas, isLoading: false });
    }

    componentDidMount() {
        this.getAllBankAccounts();
    }

    render() {

        return (
            <div>
                {renderIf(this.state.isLoading)(() => (
                    <div>
                        Loading accounts...
                    </div>
                ))}
                {renderIf(!this.state.isLoading)(() => (
                    <div>
                        {renderIf(this.state.error)(() => (
                            <div>
                                Error getting bank accounts. Please contact us if this problem persists.
                                <br />
                                {this.state.error.message}
                            </div>
                        ))}
                        {renderIf(this.state.bankAccounts)(() => (
                            <div>
                                <AccountTabs bankAccounts={this.state.bankAccounts}/>
                            </div>
                        ))}
                    </div>
                ))}

            </div>
        );
    }
}
