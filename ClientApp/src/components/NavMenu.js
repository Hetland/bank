import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import authService from './api-authorization/AuthorizeService';
import { LoginMenu } from './api-authorization/LoginMenu';
import * as renderIf from 'render-if';
import './NavMenu.css';
import AccountBalanceRoundedIcon from '@material-ui/icons/AccountBalanceRounded';

export class NavMenu extends Component {
    static displayName = NavMenu.name;

    constructor (props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar () {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    async getUser() {
        const user = await authService.getUser();
        const isAuthenticated = await authService.isAuthenticated();
        this.setState({ user, isAuthenticated });
    };

    componentDidMount() {
        this.getUser();
    }

    render() {
        return (
            <header>
                <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
                    <Container>
                        <NavbarBrand tag={Link} to="/"><AccountBalanceRoundedIcon style={{ fontSize: 30, color: "blue" }} /></NavbarBrand>  
                        {renderIf(this.state.isAuthenticated)(() => (
                            <React.Fragment>
                                <NavbarBrand tag={Link} to="/accounts">Accounts</NavbarBrand>  
                                <NavbarBrand tag={Link} to="/maketransactions">Transfer</NavbarBrand>  
                            </React.Fragment>
                        ))}
                        <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                        <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
                        <ul className="navbar-nav flex-grow">
                            <LoginMenu>
                            </LoginMenu>
                        </ul>
                        </Collapse>
                    </Container>
                </Navbar>
            </header>
        );
        }
        }
