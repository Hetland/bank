import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import * as accountTypeApi from '../../Utils/AccountTypeApiFunctions';
import * as bankAccountApi from '../../Utils/BankAccountApiFunctions';
import * as historyApi from '../../Utils/HistoryApiFunctions';
import * as transactionApi from '../../Utils/TransactionApiFunctions';

import * as profileApi from '../../Utils/ProfileApiFunctions';

import { GET_ACCOUNTTYPE, GET_ACCOUNTTYPES, POST_ACCOUNTTYPE, PUT_ACCOUNTTYPE, JOIN_ACCOUNTTYPE, LEAVE_ACCOUNTTYPE, DELETE_ACCOUNTTYPE} from '../Actions/AccountTypeActionTypes';
import { GET_BANKACCOUNT, GET_BANKACCOUNTS, POST_BANKACCOUNT, PUT_BANKACCOUNT, DELETE_BANKACCOUNT} from '../Actions/BankAccountActionTypes';
import { GET_HISTORY, GET_HISTORYS, POST_HISTORY, PUT_HISTORY, DELETE_HISTORY } from '../Actions/HistoryActionTypes';
import { GET_TRANSACTION, GET_TRANSACTIONS, POST_TRANSACTION, PUT_TRANSACTION, DELETE_TRANSACTION} from '../Actions/TransactionActionTypes';
import { GET_PROFILE, GET_PROFILES, POST_PROFILE, PUT_PROFILE, DELETE_PROFILE } from '../Actions/ProfileActionTypes';

import { postAccountTypeSuccessAction, postAccountTypeErrorAction, getAccountTypeSuccessAction, getAccountTypeErrorAction, putAccountTypeSuccessAction, putAccountTypeErrorAction, getAccountTypesSuccessAction, getAccountTypesErrorAction, joinAccountTypeSuccessAction, joinAccountTypeErrorAction, leaveAccountTypeSuccessAction, leaveAccountTypeErrorAction, deleteAccountTypeSuccessAction, deleteAccountTypeErrorAction } from '../Actions/AccountTypeActions';
import { postBankAccountSuccessAction, postBankAccountErrorAction, getBankAccountSuccessAction, getBankAccountErrorAction, getBankAccountsSuccessAction, getBankAccountsErrorAction, putBankAccountSuccessAction, putBankAccountErrorAction, deleteBankAccountSuccessAction, deleteBankAccountErrorAction } from '../Actions/BankAccountActions';
import { postHistorySuccessAction, postHistoryErrorAction, getHistorySuccessAction, getHistoryErrorAction, putHistorySuccessAction, putHistoryErrorAction, deleteHistorySuccessAction, deleteHistoryErrorAction } from '../Actions/HistoryActions';
import { postTransactionSuccessAction, postTransactionErrorAction, getTransactionSuccessAction, getTransactionErrorAction, putTransactionSuccessAction, putTransactionErrorAction, getTransactionsSuccessAction, getTransactionsErrorAction, deleteTransactionSuccessAction, deleteTransactionErrorAction } from '../Actions/TransactionActions';
import { postProfileSuccessAction, postProfileErrorAction, getProfileSuccessAction, getProfileErrorAction, putProfileSuccessAction, putProfileErrorAction, getProfilesSuccessAction, getProfilesErrorAction, deleteProfileSuccessAction, deleteProfileErrorAction } from '../Actions/ProfileActions';

import { getUserId } from '../../Utils/HelperFunctions';


function* getAccountTypes(action) {
    try {
        const accountTypes = yield call(accountTypeApi.getAccountTypes, action.id, action.endpoint);
        yield put(getAccountTypesSuccessAction(accountTypes));
    } catch (e) {
        yield put(getAccountTypesErrorAction(e));
    }
}
export function* watchGetAccountTypes() {
    yield takeEvery(GET_ACCOUNTTYPES, getAccountTypes);
}

function* getAccountType(action) {
    try {
        const accountType = yield call(accountTypeApi.getAccountType, action.id);
        yield put(getAccountTypeSuccessAction(accountType));
    } catch (e) {
        yield put(getAccountTypeErrorAction(e));
    }
}
export function* watchGetAccountType() {
    yield takeEvery(GET_ACCOUNTTYPE, getAccountType);
}

function* postAccountType(action) {
    try {
        const newAccountType = yield call(accountTypeApi.postAccountType, action.accountType);
        yield put(postAccountTypeSuccessAction(newAccountType));
    } catch (e) {
        yield put(postAccountTypeErrorAction(e));
    }
}
export function* watchAccountTypePost() {
    yield takeEvery(POST_ACCOUNTTYPE, postAccountType);
}

function* deleteAccountType(action) {
    try {
        const deletedAccountType = yield call(accountTypeApi.deleteAccountType, action.id);
        yield put(deleteAccountTypeSuccessAction(deletedAccountType));
    } catch (e) {
        yield put(deleteAccountTypeErrorAction(action.id, e));
    }
}
export function* watchDeleteAccountType() {
    yield takeEvery(DELETE_ACCOUNTTYPE, deleteAccountType);
}

function* putAccountType(action) {
    try {
        const updatedAccountType = yield call(accountTypeApi.putAccountType, action.id, action.accountType);
        yield put(putAccountTypeSuccessAction(updatedAccountType));
    } catch (e) {
        yield put(putAccountTypeErrorAction(e));
    }
}
export function* watchPutAccountType() {
    yield takeEvery(PUT_ACCOUNTTYPE, putAccountType);
}

function* joinAccountType(action) {
    try {
        const updatedAccountType = yield call(accountTypeApi.joinAccountType, action.id, action.userIds);
        yield put(joinAccountTypeSuccessAction(updatedAccountType));
    } catch (e) {
        yield put(joinAccountTypeErrorAction(e));
    }
}
export function* watchJoinAccountType() {
    yield takeEvery(JOIN_ACCOUNTTYPE, joinAccountType);
}

function* leaveAccountType(action) {
    try {
        const updatedAccountType = yield call(accountTypeApi.leaveAccountType, action.id);
        yield put(leaveAccountTypeSuccessAction(updatedAccountType));
    } catch (e) {
        yield put(leaveAccountTypeErrorAction(e));
    }
}
export function* watchLeaveAccountType() {
    yield takeEvery(LEAVE_ACCOUNTTYPE, leaveAccountType);
}




function* getBankAccounts(action) {
    try {
        const bankAccounts = yield call(bankAccountApi.getBankAccounts);
        yield put(getBankAccountsSuccessAction(bankAccounts));
    } catch (e) {
        yield put(getBankAccountsErrorAction(e));
    }
}
// change to takeLatest if including dynamic filtering/searching
export function* watchGetBankAccounts() {
    yield takeEvery(GET_BANKACCOUNTS, getBankAccounts);
}

function* getBankAccount(action) {
    try {
        const bankAccount = yield call(bankAccountApi.getBankAccount, action.value);
        yield put(getBankAccountSuccessAction(bankAccount));
    } catch (e) {
        yield put(getBankAccountErrorAction(e));
    }
}
// change to takeLatest if including dynamic filtering/searching
export function* watchGetBankAccount() {
    yield takeEvery(GET_BANKACCOUNT, getBankAccount);
}

function* postBankAccount(action) {
    try {
        const newBankAccount = yield call(bankAccountApi.postBankAccount, action.value);
        yield put(postBankAccountSuccessAction(newBankAccount));
    } catch (e) {
        yield put(postBankAccountErrorAction(e));
    }
}
export function* watchBankAccountPost() {
    yield takeEvery(POST_BANKACCOUNT, postBankAccount);
}

function* deleteBankAccount(action) {
    try {
        const deletedBankAccount = yield call(bankAccountApi.deleteBankAccount, action.id);
        yield put(deleteBankAccountSuccessAction(deletedBankAccount));
    } catch (e) {
        yield put(deleteBankAccountErrorAction(action.id, e));
    }
}
export function* watchDeleteBankAccount() {
    yield takeEvery(DELETE_BANKACCOUNT, deleteBankAccount);
}

function* putBankAccount(action) {
    try {
        const updatedBankAccount = yield call(bankAccountApi.putBankAccount, action.id, action.bankAccount);
        yield put(putBankAccountSuccessAction(updatedBankAccount));
    } catch (e) {
        yield put(putBankAccountErrorAction(e));
    }
}
export function* watchPutBankAccount() {
    yield takeEvery(PUT_BANKACCOUNT, putBankAccount);
}






function* getHistory(action) {
    try {
        const history = yield call(historyApi.getHistory, action.value);
        yield put(getHistorySuccessAction(history));
    } catch (e) {
        yield put(getHistoryErrorAction(e));
    }
}
// change to takeLatest if including dynamic filtering/searching
export function* watchGetHistory() {
    yield takeEvery(GET_HISTORY, getHistory);
}

function* postHistory(action) {
    try {
        const newHistory = yield call(historyApi.postHistory, action.value);
        yield put(postHistorySuccessAction(newHistory));
    } catch (e) {
        yield put(postHistoryErrorAction(e));
    }
}
export function* watchHistoryPost() {
    yield takeEvery(POST_HISTORY, postHistory);
}

function* deleteHistory(action) {
    try {
        const deletedHistory = yield call(historyApi.deleteHistory, action.id);
        yield put(deleteHistorySuccessAction(deletedHistory));
    } catch (e) {
        yield put(deleteHistoryErrorAction(action.id, e));
    }
}
export function* watchDeleteHistory() {
    yield takeEvery(DELETE_HISTORY, deleteHistory);
}

function* putHistory(action) {
    try {
        const updatedHistory = yield call(historyApi.putHistory, action.id, action.bankAccount);
        yield put(putHistorySuccessAction(updatedHistory));
    } catch (e) {
        yield put(putHistoryErrorAction(e));
    }
}
export function* watchPutHistory() {
    yield takeEvery(PUT_HISTORY, putHistory);
}




function* getTransactions(action) {
    try {
        const transactions = yield call(transactionApi.getTransactions);
        yield put(getTransactionsSuccessAction(transactions));
    } catch (e) {
        yield put(getTransactionsErrorAction(e));
    }
}
// change to takeLatest if including dynamic filtering/searching
export function* watchGetTransactions() {
    yield takeEvery(GET_TRANSACTIONS, getTransactions);
}


function* getTransaction(action) {
    try {
        const transaction = yield call(transactionApi.getTransaction, action.value);
        yield put(getTransactionSuccessAction(transaction));
    } catch (e) {
        yield put(getTransactionErrorAction(e));
    }
}
// change to takeLatest if including dynamic filtering/searching
export function* watchGetTransaction() {
    yield takeEvery(GET_TRANSACTION, getTransaction);
}

function* postTransaction(action) {
    try {
        const newTransaction = yield call(transactionApi.postTransaction, action.value);
        yield put(postTransactionSuccessAction(newTransaction));
    } catch (e) {
        yield put(postTransactionErrorAction(e));
    }
}
export function* watchTransactionPost() {
    yield takeEvery(POST_TRANSACTION, postTransaction);
}

function* deleteTransaction(action) {
    try {
        const deletedTransaction = yield call(transactionApi.deleteTransaction, action.id);
        yield put(deleteTransactionSuccessAction(deletedTransaction));
    } catch (e) {
        yield put(deleteTransactionErrorAction(action.id, e));
    }
}
export function* watchDeleteTransaction() {
    yield takeEvery(DELETE_TRANSACTION, deleteTransaction);
}

function* putTransaction(action) {
    try {
        const updatedTransaction = yield call(transactionApi.putTransaction, action.id, action.bankAccount);
        yield put(putTransactionSuccessAction(updatedTransaction));
    } catch (e) {
        yield put(putTransactionErrorAction(e));
    }
}
export function* watchPutTransaction() {
    yield takeEvery(PUT_TRANSACTION, putTransaction);
}



function* getProfiles(action) {
    try {
        const profiles = yield call(profileApi.getProfiles);
        yield put(getProfilesSuccessAction(profiles));
    } catch (e) {
        yield put(getProfilesErrorAction(e));
    }
}
export function* watchGetProfiles() {
    yield takeLatest(GET_PROFILES, getProfiles);
}

function* getProfile(action) {
    try {
        const profile = yield call(profileApi.getProfile, action.id);
        yield put(getProfileSuccessAction(profile));
    } catch (e) {
        yield put(getProfileErrorAction(e));
    }
}
export function* watchGetProfile() {
    yield takeEvery(GET_PROFILE, getProfile);
}

function* putProfile(action) {
    try {
        const updatedProfile = yield call(profileApi.putProfile, action.id, action.profile);
        yield put(putProfileSuccessAction(updatedProfile));
    } catch (e) {
        yield put(putProfileErrorAction(e));
    }
}
export function* watchPutProfile() {
    yield takeEvery(PUT_PROFILE, putProfile);
}

function* postProfile(action) {
    try {
        const newProfile = yield call(profileApi.postProfile, action.profile);
        yield put(postProfileSuccessAction(newProfile));
    } catch (e) {
        yield put(postProfileErrorAction(e));
    }
}
export function* watchPostProfile() {
    yield takeEvery(POST_PROFILE, postProfile);
}

function* deleteProfile(action) {
    try {
        const deletedProfile = yield call(profileApi.deleteProfile, action.id);
        yield put(deleteProfileSuccessAction(deletedProfile));
    } catch (e) {
        yield put(deleteProfileErrorAction(action.id, e));
    }
}
export function* watchDeleteProfile() {
    yield takeEvery(DELETE_PROFILE, deleteProfile);
}
