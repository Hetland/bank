﻿import { bindActionCreators, ActionCreatorsMapObject } from 'redux';
import {
    getAccountTypesAction, getAccountTypesSuccessAction, getAccountTypesErrorAction,
    getAccountTypeAction, getAccountTypeSuccessAction, getAccountTypeErrorAction,
    postAccountTypeAction, postAccountTypeSuccessAction, postAccountTypeErrorAction,
    joinAccountTypeAction, joinAccountTypeSuccessAction, joinAccountTypeErrorAction,
    leaveAccountTypeAction, leaveAccountTypeSuccessAction, leaveAccountTypeErrorAction,
    deleteAccountTypeAction, deleteAccountTypeSuccessAction, deleteAccountTypeErrorAction,
    putAccountTypeAction, putAccountTypeSuccessAction, putAccountTypeErrorAction,
} from './AccountTypeActions';
import store from '../Store/Store';


const actions = {
    getAccountTypesAction,
    getAccountTypesSuccessAction,
    getAccountTypesErrorAction,
    getAccountTypeAction,
    getAccountTypeSuccessAction,
    getAccountTypeErrorAction,
    postAccountTypeAction,
    postAccountTypeSuccessAction,
    postAccountTypeErrorAction,
    joinAccountTypeAction,
    joinAccountTypeSuccessAction,
    joinAccountTypeErrorAction,
    leaveAccountTypeAction,
    leaveAccountTypeSuccessAction,
    leaveAccountTypeErrorAction,
    deleteAccountTypeAction,
    deleteAccountTypeSuccessAction,
    deleteAccountTypeErrorAction,
    putAccountTypeAction,
    putAccountTypeSuccessAction,
    putAccountTypeErrorAction,
}

const accounttypeActionDispatchers = bindActionCreators(actions, store.dispatch);

export default accounttypeActionDispatchers;
