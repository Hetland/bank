﻿import { bindActionCreators, ActionCreatorsMapObject } from 'redux';
import {
    getBankAccountsAction, getBankAccountsSuccessAction, getBankAccountsErrorAction,
    getBankAccountAction, getBankAccountSuccessAction, getBankAccountErrorAction,
    postBankAccountAction, postBankAccountSuccessAction, postBankAccountErrorAction,
    deleteBankAccountAction, deleteBankAccountSuccessAction, deleteBankAccountErrorAction,
    putBankAccountAction, putBankAccountSuccessAction, putBankAccountErrorAction,
} from './BankAccountActions';
import store from '../Store/Store';


const actions = {
    getBankAccountsAction,
    getBankAccountsSuccessAction,
    getBankAccountsErrorAction,
    getBankAccountAction,
    getBankAccountSuccessAction,
    getBankAccountErrorAction,
    postBankAccountAction,
    postBankAccountSuccessAction,
    postBankAccountErrorAction,
    deleteBankAccountAction,
    deleteBankAccountSuccessAction,
    deleteBankAccountErrorAction,
    putBankAccountAction,
    putBankAccountSuccessAction,
    putBankAccountErrorAction,
}

const bankaccountActionDispatchers = bindActionCreators(actions, store.dispatch);

export default bankaccountActionDispatchers;
