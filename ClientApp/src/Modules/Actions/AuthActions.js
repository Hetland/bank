﻿import { USER_LOGIN, USER_LOGIN_SUCCESS, USER_LOGIN_ERROR, USER_LOGOUT } from './AuthActionTypes';

export const userLoginAction = (isAuthenticated, user) => ({
    type: USER_LOGIN,
    isAuthenticated,
    user
});

export const userLogoutAction = () => ({
    type: USER_LOGOUT
});