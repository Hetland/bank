﻿import {
    GET_ACCOUNTTYPES, GET_ACCOUNTTYPES_SUCCESS, GET_ACCOUNTTYPES_ERROR,
    POST_ACCOUNTTYPE, POST_ACCOUNTTYPE_SUCCESS, POST_ACCOUNTTYPE_ERROR,
    JOIN_ACCOUNTTYPE, JOIN_ACCOUNTTYPE_SUCCESS, JOIN_ACCOUNTTYPE_ERROR,
    DELETE_ACCOUNTTYPE, DELETE_ACCOUNTTYPE_SUCCESS, DELETE_ACCOUNTTYPE_ERROR,
    PUT_ACCOUNTTYPE, PUT_ACCOUNTTYPE_SUCCESS, PUT_ACCOUNTTYPE_ERROR,
    GET_ACCOUNTTYPE, GET_ACCOUNTTYPE_SUCCESS, GET_ACCOUNTTYPE_ERROR,
    LEAVE_ACCOUNTTYPE, LEAVE_ACCOUNTTYPE_SUCCESS, LEAVE_ACCOUNTTYPE_ERROR
} from "./AccountTypeActionTypes";

export const getAccountTypesAction = () => ({
    type: GET_ACCOUNTTYPES
});

export const getAccountTypesSuccessAction = (accounttypes) => ({
    type: GET_ACCOUNTTYPES_SUCCESS,
    value: accounttypes
});

export const getAccountTypesErrorAction = (error) => ({
    type: GET_ACCOUNTTYPES_ERROR,
    error
});



export const getAccountTypeAction = (accounttypeId) => ({
    type: GET_ACCOUNTTYPE,
    value: accounttypeId
});

export const getAccountTypeSuccessAction = (accounttype) => ({
    type: GET_ACCOUNTTYPE_SUCCESS,
    value: accounttype
});

export const getAccountTypeErrorAction = (error) => ({
    type: GET_ACCOUNTTYPE_ERROR,
    error
});



export const postAccountTypeAction = accounttype => ({
    type: POST_ACCOUNTTYPE,
    value: accounttype
});

export const postAccountTypeSuccessAction = (accounttype) => ({
    type: POST_ACCOUNTTYPE_SUCCESS,
    value: accounttype
});

export const postAccountTypeErrorAction = (error) => ({
    type: POST_ACCOUNTTYPE_ERROR,
    error
});



export const joinAccountTypeAction = (id, accountId) => ({
    type: JOIN_ACCOUNTTYPE,
    id,
    accountId
});

export const joinAccountTypeSuccessAction = (accounttype) => ({
    type: JOIN_ACCOUNTTYPE_SUCCESS,
    accounttype
});

export const joinAccountTypeErrorAction = (id, error) => ({
    type: JOIN_ACCOUNTTYPE_ERROR,
    id,
    error
});


export const leaveAccountTypeAction = (id) => ({
    type: LEAVE_ACCOUNTTYPE,
    id
});

export const leaveAccountTypeSuccessAction = (accounttype) => ({
    type: LEAVE_ACCOUNTTYPE_SUCCESS,
    accounttype
});

export const leaveAccountTypeErrorAction = (id, error) => ({
    type: LEAVE_ACCOUNTTYPE_ERROR,
    id,
    error
});



export const deleteAccountTypeAction = id => ({
    type: DELETE_ACCOUNTTYPE,
    value: id
});

export const deleteAccountTypeSuccessAction = (accounttype) => ({
    type: DELETE_ACCOUNTTYPE_SUCCESS,
    accounttype
});

export const deleteAccountTypeErrorAction = (id, error) => ({
    type: DELETE_ACCOUNTTYPE_ERROR,
    id,
    error
});



export const putAccountTypeAction = (id, accounttype) => ({
    type: PUT_ACCOUNTTYPE,
    value: { id, accounttype }
});

export const putAccountTypeSuccessAction = (accounttype) => ({
    type: PUT_ACCOUNTTYPE_SUCCESS,
    accounttype
});

export const putAccountTypeErrorAction = (id, error) => ({
    type: LEAVE_ACCOUNTTYPE_ERROR,
    id,
    error
});