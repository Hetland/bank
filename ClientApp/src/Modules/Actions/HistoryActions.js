﻿import {
    POST_HISTORY, POST_HISTORY_SUCCESS, POST_HISTORY_ERROR,
    DELETE_HISTORY, DELETE_HISTORY_SUCCESS, DELETE_HISTORY_ERROR,
    PUT_HISTORY, PUT_HISTORY_SUCCESS, PUT_HISTORY_ERROR,
    GET_HISTORY, GET_HISTORY_SUCCESS, GET_HISTORY_ERROR
} from "./HistoryActionTypes";



export const getHistoryAction = (historyId) => ({
    type: GET_HISTORY,
    value: historyId
});

export const getHistorySuccessAction = (history) => ({
    type: GET_HISTORY_SUCCESS,
    value: history
});

export const getHistoryErrorAction = (error) => ({
    type: GET_HISTORY_ERROR,
    error
});



export const postHistoryAction = history => ({
    type: POST_HISTORY,
    value: history
});

export const postHistorySuccessAction = (history) => ({
    type: POST_HISTORY_SUCCESS,
    value: history
});

export const postHistoryErrorAction = (error) => ({
    type: POST_HISTORY_ERROR,
    error
});



export const deleteHistoryAction = id => ({
    type: DELETE_HISTORY,
    value: id
});

export const deleteHistorySuccessAction = (history) => ({
    type: DELETE_HISTORY_SUCCESS,
    history
});

export const deleteHistoryErrorAction = (id, error) => ({
    type: DELETE_HISTORY_ERROR,
    id,
    error
});



export const putHistoryAction = (id, history) => ({
    type: PUT_HISTORY,
    value: { id, history }
});

export const putHistorySuccessAction = (history) => ({
    type: PUT_HISTORY_SUCCESS,
    history
});

export const putHistoryErrorAction = (id, error) => ({
    type: PUT_HISTORY_ERROR,
    id,
    error
});
