﻿import { bindActionCreators, ActionCreatorsMapObject } from 'redux';
import {
    getHistorysAction, getHistorysSuccessAction, getHistorysErrorAction,
    getHistoryAction, getHistorySuccessAction, getHistoryErrorAction,
    postHistoryAction, postHistorySuccessAction, postHistoryErrorAction,
    deleteHistoryAction,
    putHistoryAction,
} from './HistoryActions';
import store from '../Store/Store';


const actions = {
    getHistorysAction,
    getHistorysSuccessAction,
    getHistorysErrorAction,
    getHistoryAction,
    getHistorySuccessAction,
    getHistoryErrorAction,
    postHistoryAction,
    postHistorySuccessAction,
    postHistoryErrorAction,
    deleteHistoryAction,
    putHistoryAction
}

const historyActionDispatchers = bindActionCreators(actions, store.dispatch);

export default historyActionDispatchers;
