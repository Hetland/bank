﻿import { bindActionCreators, ActionCreatorsMapObject } from 'redux';
import { userLoginAction, userLogoutAction } from './AuthActions';
import store from '../Store/Store';


const actions = {
    userLoginAction,
    userLogoutAction
}

const authActionDispatchers = bindActionCreators(actions, store.dispatch);

export default authActionDispatchers;