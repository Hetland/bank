﻿import { bindActionCreators, ActionCreatorsMapObject } from 'redux';
import {
    getProfilesAction, getProfilesSuccessAction, getProfilesErrorAction,
    getProfileAction, getProfileSuccessAction, getProfileErrorAction,
    postProfileAction, postProfileSuccessAction, postProfileErrorAction,
    deleteProfileAction,
    putProfileAction,
} from './ProfileActions';
import store from '../Store/Store';


const actions = {
    getProfilesAction,
    getProfilesSuccessAction,
    getProfilesErrorAction,
    getProfileAction,
    getProfileSuccessAction,
    getProfileErrorAction,
    postProfileAction,
    postProfileSuccessAction,
    postProfileErrorAction,
    deleteProfileAction,
    putProfileAction
}

const profileActionDispatchers = bindActionCreators(actions, store.dispatch);

export default profileActionDispatchers;
