﻿import {
    GET_PROFILES, GET_PROFILES_SUCCESS, GET_PROFILES_ERROR,
    POST_PROFILE, POST_PROFILE_SUCCESS, POST_PROFILE_ERROR,
    DELETE_PROFILE, DELETE_PROFILE_SUCCESS, DELETE_PROFILE_ERROR,
    PUT_PROFILE, PUT_PROFILE_SUCCESS, PUT_PROFILE_ERROR,
    GET_PROFILE, GET_PROFILE_SUCCESS, GET_PROFILE_ERROR
} from "./ProfileActionTypes";

export const getProfilesAction = () => ({
    type: GET_PROFILES
});

export const getProfilesSuccessAction = (profiles) => ({
    type: GET_PROFILES_SUCCESS,
    value: profiles
});

export const getProfilesErrorAction = (error) => ({
    type: GET_PROFILES_ERROR,
    error
});



export const getProfileAction = (profileId) => ({
    type: GET_PROFILE,
    value: profileId
});

export const getProfileSuccessAction = (profile) => ({
    type: GET_PROFILE_SUCCESS,
    value: profile
});

export const getProfileErrorAction = (error) => ({
    type: GET_PROFILE_ERROR,
    error
});



export const postProfileAction = profile => ({
    type: POST_PROFILE,
    value: profile
});

export const postProfileSuccessAction = (profile) => ({
    type: POST_PROFILE_SUCCESS,
    value: profile
});

export const postProfileErrorAction = (error) => ({
    type: POST_PROFILE_ERROR,
    error
});



export const deleteProfileAction = id => ({
    type: DELETE_PROFILE,
    value: id
});

export const deleteProfileSuccessAction = (profile) => ({
    type: DELETE_PROFILE_SUCCESS,
    value: profile
});

export const deleteProfileErrorAction = (error) => ({
    type: DELETE_PROFILE_ERROR,
    error
});



export const putProfileAction = (id, profile) => ({
    type: PUT_PROFILE,
    value: { id, profile }
});

export const putProfileSuccessAction = (profile) => ({
    type: PUT_PROFILE_SUCCESS,
    value: profile
});

export const putProfileErrorAction = (error) => ({
    type: PUT_PROFILE_ERROR,
    error
});