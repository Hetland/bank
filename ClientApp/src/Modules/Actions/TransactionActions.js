﻿import {
    GET_TRANSACTIONS, GET_TRANSACTIONS_SUCCESS, GET_TRANSACTIONS_ERROR,
    POST_TRANSACTION, POST_TRANSACTION_SUCCESS, POST_TRANSACTION_ERROR,
    DELETE_TRANSACTION, DELETE_TRANSACTION_SUCCESS, DELETE_TRANSACTION_ERROR,
    PUT_TRANSACTION, PUT_TRANSACTION_SUCCESS, PUT_TRANSACTION_ERROR,
    GET_TRANSACTION, GET_TRANSACTION_SUCCESS, GET_TRANSACTION_ERROR,
} from "./TransactionActionTypes";

export const getTransactionsAction = () => ({
    type: GET_TRANSACTIONS
});

export const getTransactionsSuccessAction = (transactions) => ({
    type: GET_TRANSACTIONS_SUCCESS,
    value: transactions
});

export const getTransactionsErrorAction = (error) => ({
    type: GET_TRANSACTIONS_ERROR,
    error
});



export const getTransactionAction = (transactionId) => ({
    type: GET_TRANSACTION,
    value: transactionId
});

export const getTransactionSuccessAction = (transaction) => ({
    type: GET_TRANSACTION_SUCCESS,
    value: transaction
});

export const getTransactionErrorAction = (error) => ({
    type: GET_TRANSACTION_ERROR,
    error
});



export const postTransactionAction = transaction => ({
    type: POST_TRANSACTION,
    value: transaction
});

export const postTransactionSuccessAction = (transaction) => ({
    type: POST_TRANSACTION_SUCCESS,
    value: transaction
});

export const postTransactionErrorAction = (error) => ({
    type: POST_TRANSACTION_ERROR,
    error
});



export const deleteTransactionAction = id => ({
    type: DELETE_TRANSACTION,
    value: id
});

export const deleteTransactionSuccessAction = (transaction) => ({
    type: DELETE_TRANSACTION_SUCCESS,
    value: transaction
});

export const deleteTransactionErrorAction = (error) => ({
    type: DELETE_TRANSACTION_ERROR,
    error
});



export const putTransactionAction = (id, transaction) => ({
    type: PUT_TRANSACTION,
    value: { id, transaction }
});

export const putTransactionSuccessAction = (transaction) => ({
    type: PUT_TRANSACTION_SUCCESS,
    value: transaction
});

export const putTransactionErrorAction = (error) => ({
    type: PUT_TRANSACTION_ERROR,
    error
});
