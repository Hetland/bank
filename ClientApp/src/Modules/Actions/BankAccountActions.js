﻿import {
    GET_BANKACCOUNTS, GET_BANKACCOUNTS_SUCCESS, GET_BANKACCOUNTS_ERROR,
    POST_BANKACCOUNT, POST_BANKACCOUNT_SUCCESS, POST_BANKACCOUNT_ERROR,
    DELETE_BANKACCOUNT, DELETE_BANKACCOUNT_SUCCESS, DELETE_BANKACCOUNT_ERROR,
    PUT_BANKACCOUNT, PUT_BANKACCOUNT_SUCCESS, PUT_BANKACCOUNT_ERROR,
    GET_BANKACCOUNT, GET_BANKACCOUNT_SUCCESS, GET_BANKACCOUNT_ERROR
} from "./BankAccountActionTypes";

export const getBankAccountsAction = () => ({
    type: GET_BANKACCOUNTS
});

export const getBankAccountsSuccessAction = (bankaccounts) => ({
    type: GET_BANKACCOUNTS_SUCCESS,
    value: bankaccounts
});

export const getBankAccountsErrorAction = (error) => ({
    type: GET_BANKACCOUNTS_ERROR,
    error
});



export const getBankAccountAction = (bankaccountId) => ({
    type: GET_BANKACCOUNT,
    value: bankaccountId
});

export const getBankAccountSuccessAction = (bankaccount) => ({
    type: GET_BANKACCOUNT_SUCCESS,
    value: bankaccount
});

export const getBankAccountErrorAction = (error) => ({
    type: GET_BANKACCOUNT_ERROR,
    error
});



export const postBankAccountAction = bankaccount => ({
    type: POST_BANKACCOUNT,
    value: bankaccount
});

export const postBankAccountSuccessAction = (bankaccount) => ({
    type: POST_BANKACCOUNT_SUCCESS,
    value: bankaccount
});

export const postBankAccountErrorAction = (error) => ({
    type: POST_BANKACCOUNT_ERROR,
    error
});



export const deleteBankAccountAction = id => ({
    type: DELETE_BANKACCOUNT,
    value: id
});

export const deleteBankAccountSuccessAction = (bankaccount) => ({
    type: DELETE_BANKACCOUNT_SUCCESS,
    bankaccount
});

export const deleteBankAccountErrorAction = (id, error) => ({
    type: DELETE_BANKACCOUNT_ERROR,
    id,
    error
});



export const putBankAccountAction = (id, bankaccount) => ({
    type: PUT_BANKACCOUNT,
    value: { id, bankaccount }
});

export const putBankAccountSuccessAction = (bankaccount) => ({
    type: PUT_BANKACCOUNT_SUCCESS,
    bankaccount
});

export const putBankAccountErrorAction = (id, error) => ({
    type: PUT_BANKACCOUNT_ERROR,
    id,
    error
});
