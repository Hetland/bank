﻿import { bindActionCreators, ActionCreatorsMapObject } from 'redux';
import {
    getTransactionsAction, getTransactionsSuccessAction, getTransactionsErrorAction,
    getTransactionAction, getTransactionSuccessAction, getTransactionErrorAction,
    postTransactionAction, postTransactionSuccessAction, postTransactionErrorAction,
    deleteTransactionAction,
    putTransactionAction,
} from './TransactionActions';
import store from '../Store/Store';


const actions = {
    getTransactionsAction,
    getTransactionsSuccessAction,
    getTransactionsErrorAction,
    getTransactionAction,
    getTransactionSuccessAction,
    getTransactionErrorAction,
    postTransactionAction,
    postTransactionSuccessAction,
    postTransactionErrorAction,
    deleteTransactionAction,
    putTransactionAction
}

const transactionActionDispatchers = bindActionCreators(actions, store.dispatch);

export default transactionActionDispatchers;
