﻿import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import accountTypeReducer from '../Reducers/AccountTypeReducer';
import bankAccountReducer from '../Reducers/BankAccountReducer';
import historyReducer from '../Reducers/HistoryReducer';
import transactionReducer from '../Reducers/TransactionReducer';
import profileReducer from '../Reducers/ProfileReducer';
import authReducer from '../Reducers/AuthReducer';
import root from './Index';

const sagaMiddleware = createSagaMiddleware();

const reducers = combineReducers({
    accountTypeState: accountTypeReducer,
    bankAccountState: bankAccountReducer,
    historyState: historyReducer,
    transactionState: transactionReducer, 
    authState: authReducer,
    profileState: profileReducer
});

const store = createStore(reducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(root)
export default store;
