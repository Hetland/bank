﻿import { fork } from 'redux-saga/effects';
import {
    watchGetAccountTypes,
    watchGetAccountType,
    watchJoinAccountType,
    watchLeaveAccountType,
    watchPutAccountType,
    watchDeleteAccountType,

    watchGetBankAccounts,
    watchGetBankAccount,
    watchPutBankAccount,
    watchDeleteBankAccount,

    watchGetHistory,
    watchPutHistory,
    watchDeleteHistory,

    watchGetProfiles,
    watchGetProfile,
    watchPutProfile,
    watchDeleteProfile,

    watchGetTransactions,
    watchGetTransaction,
    watchPutTransaction,
    watchDeleteTransaction,
} from '../Sagas/BankApiSaga';

function* root() {
    yield fork(watchGetAccountTypes);
    yield fork(watchGetAccountType);
    yield fork(watchJoinAccountType);
    yield fork(watchLeaveAccountType);
    yield fork(watchPutAccountType);
    yield fork(watchDeleteAccountType);

    yield fork(watchGetBankAccounts);
    yield fork(watchGetBankAccount);
    yield fork(watchPutBankAccount);
    yield fork(watchDeleteBankAccount);

    yield fork(watchGetHistory);
    yield fork(watchPutHistory);
    yield fork(watchDeleteHistory);

    yield fork(watchGetProfiles);
    yield fork(watchGetProfile);
    yield fork(watchPutProfile);
    yield fork(watchDeleteProfile);

    yield fork(watchGetTransactions);
    yield fork(watchGetTransaction);
    yield fork(watchPutTransaction);
    yield fork(watchDeleteTransaction);


}

export default root;
