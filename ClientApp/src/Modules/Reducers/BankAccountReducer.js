﻿import {
    POST_BANKACCOUNT, POST_BANKACCOUNT_SUCCESS, POST_BANKACCOUNT_ERROR,
    DELETE_BANKACCOUNT,
    PUT_BANKACCOUNT,
    GET_BANKACCOUNTS, GET_BANKACCOUNTS_SUCCESS, GET_BANKACCOUNTS_ERROR,
    GET_BANKACCOUNT, GET_BANKACCOUNT_SUCCESS, GET_BANKACCOUNT_ERROR,
} from "../Actions/BankAccountActionTypes";
import * as _ from 'lodash';
import * as update from 'immutability-helper';
import { concatItemsWithId } from "../../Utils/HelperFunctions";

const initialState = {
    bankaccounts: [],
    isLoading: false,
    isCreatingNewBankAccount: false,
    error: null
}

const bankaccountReducer = function (state = initialState, action) {
    switch (action.type) {
        case GET_BANKACCOUNTS:
            return { ...state, isLoading: true }

        case GET_BANKACCOUNTS_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                bankaccounts: { $apply: (bankaccounts) => concatItemsWithId(bankaccounts, action.value) }
            });

        case GET_BANKACCOUNTS_ERROR:
            return { ...state, isLoading: false, error: action.error }




        case GET_BANKACCOUNT:
            return { ...state, isLoading: true }

        case GET_BANKACCOUNT_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                bankaccounts: { $apply: (bankaccounts) => concatItemsWithId(bankaccounts, [action.value]) }
            });

        case GET_BANKACCOUNT_ERROR:
            return { ...state, isLoading: false, error: action.error }



        case POST_BANKACCOUNT:
            return { ...state, isCreatingNewBankAccount: true };

        case POST_BANKACCOUNT_SUCCESS:
            const newBankAccounts = _.cloneDeep(state.bankaccounts);
            newBankAccounts.unshift(action.value);
            return { ...state, bankaccounts: newBankAccounts, isCreatingNewBankAccount: false }

        case POST_BANKACCOUNT_ERROR:
            return { ...state, isCreatingNewBankAccount: false, error: action.error }



        case DELETE_BANKACCOUNT:
            throw Error('deleteBankAccount is not implemented!');

        case PUT_BANKACCOUNT:
            throw Error('putBankAccount is not implemented!');

        default:
            return state;
    }
};

export default bankaccountReducer;
