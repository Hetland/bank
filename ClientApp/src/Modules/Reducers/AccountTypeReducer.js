﻿import {
    POST_ACCOUNTTYPE, POST_ACCOUNTTYPE_SUCCESS, POST_ACCOUNTTYPE_ERROR,
    DELETE_ACCOUNTTYPE,
    PUT_ACCOUNTTYPE,
    GET_ACCOUNTTYPES, GET_ACCOUNTTYPES_SUCCESS, GET_ACCOUNTTYPES_ERROR,
    GET_ACCOUNTTYPE, GET_ACCOUNTTYPE_SUCCESS, GET_ACCOUNTTYPE_ERROR,
    JOIN_ACCOUNTTYPE, JOIN_ACCOUNTTYPE_SUCCESS, JOIN_ACCOUNTTYPE_ERROR,
    LEAVE_ACCOUNTTYPE, LEAVE_ACCOUNTTYPE_SUCCESS, LEAVE_ACCOUNTTYPE_ERROR,
} from "../Actions/AccountTypeActionTypes";
import * as _ from 'lodash';
import * as update from 'immutability-helper';
import { concatItemsWithId } from "../../Utils/HelperFunctions";

const initialState = {
    accounttypes: [],
    isLoading: false,
    isCreatingNewAccountType: false,
    error: null
}

const accounttypeReducer = function (state = initialState, action) {
    switch (action.type) {
        case GET_ACCOUNTTYPES:
            return { ...state, isLoading: true }

        case GET_ACCOUNTTYPES_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                accounttypes: { $apply: (accounttypes) => concatItemsWithId(accounttypes, action.value) }
            });

        case GET_ACCOUNTTYPES_ERROR:
            return { ...state, isLoading: false, error: action.error }




        case GET_ACCOUNTTYPE:
            return { ...state, isLoading: true }

        case GET_ACCOUNTTYPE_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                accounttypes: { $apply: (accounttypes) => concatItemsWithId(accounttypes, [action.value]) }
            });

        case GET_ACCOUNTTYPE_ERROR:
            return { ...state, isLoading: false, error: action.error }



        case POST_ACCOUNTTYPE:
            return { ...state, isCreatingNewAccountType: true };

        case POST_ACCOUNTTYPE_SUCCESS:
            const newAccountTypes = _.cloneDeep(state.accounttypes);
            newAccountTypes.unshift(action.value);
            return { ...state, accounttypes: newAccountTypes, isCreatingNewAccountType: false }

        case POST_ACCOUNTTYPE_ERROR:
            return { ...state, isCreatingNewAccountType: false, error: action.error }



        case JOIN_ACCOUNTTYPE:
        case LEAVE_ACCOUNTTYPE:
            return update(state, {
                accounttypes: {
                    $apply: (accounttypes) => {
                        return accounttypes.map(at => {
                            if (at.id === action.id) {
                                return update(at, {
                                    isUpdating: { $set: true }
                                });
                            }
                            return at;
                        });
                    }
                }
            });

        case JOIN_ACCOUNTTYPE_SUCCESS:
        case LEAVE_ACCOUNTTYPE_SUCCESS:
            return update(state, {
                accounttypes: {
                    $apply: (accounttypes) => {
                        return accounttypes.map(at => {
                            if (at.id === action.accounttype.id) {
                                return action.accounttype;
                            }
                            return at;
                        });
                    }
                }
            });

        case JOIN_ACCOUNTTYPE_ERROR:
        case LEAVE_ACCOUNTTYPE_ERROR:
            return update(state, {
                accounttypes: {
                    $apply: (accounttypes) => {
                        return accounttypes.map(at => {
                            if (at.id === action.id) {
                                return update(at, {
                                    isUpdating: { $set: false },
                                    updateError: { $set: action.error }
                                });
                            }
                            return at;
                        });
                    }
                }
            });

        case DELETE_ACCOUNTTYPE:
            throw Error('deleteAccountType is not implemented!');

        case PUT_ACCOUNTTYPE:
            throw Error('putAccountType is not implemented!');

        default:
            return state;
    }
};

export default accounttypeReducer;
