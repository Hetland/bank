﻿import {
    POST_TRANSACTION, POST_TRANSACTION_SUCCESS, POST_TRANSACTION_ERROR,
    DELETE_TRANSACTION,
    PUT_TRANSACTION,
    GET_TRANSACTIONS, GET_TRANSACTIONS_SUCCESS, GET_TRANSACTIONS_ERROR,
    GET_TRANSACTION, GET_TRANSACTION_SUCCESS, GET_TRANSACTION_ERROR
} from "../Actions/TransactionActionTypes";
import * as _ from 'lodash';
import * as update from 'immutability-helper';
import { concatItemsWithId } from "../../Utils/HelperFunctions";

const initialState = {
    transactions: [],
    isLoading: false,
    isCreatingNewTransaction: false,
    error: null
}

const transactionReducer = function (state = initialState, action) {
    switch (action.type) {
        case GET_TRANSACTIONS:
            return { ...state, isLoading: true }

        case GET_TRANSACTIONS_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                transactions: { $apply: (transactions) => concatItemsWithId(transactions, action.value) }
            });

        case GET_TRANSACTIONS_ERROR:
            return { ...state, isLoading: false, error: action.error }




        case GET_TRANSACTION:
            return { ...state, isLoading: true }

        case GET_TRANSACTION_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                transactions: { $apply: (transactions) => concatItemsWithId(transactions, [action.value]) }
            });

        case GET_TRANSACTION_ERROR:
            return { ...state, isLoading: false, error: action.error }



        case POST_TRANSACTION:
            return { ...state, isCreatingNewTransaction: true };

        case POST_TRANSACTION_SUCCESS:
            const newTransactions = _.cloneDeep(state.transactions);
            newTransactions.unshift(action.value);
            return { ...state, transactions: newTransactions, isCreatingNewTransaction: false }

        case POST_TRANSACTION_ERROR:
            return { ...state, isCreatingNewTransaction: false, error: action.error }



        case DELETE_TRANSACTION:
            throw Error('deleteTransaction is not implemented!');

        case PUT_TRANSACTION:
            throw Error('putTransaction is not implemented!');

        default:
            return state;
    }
};

export default transactionReducer;
