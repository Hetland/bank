﻿import * as _ from 'lodash';
import * as update from 'immutability-helper';
import authService from '../../components/api-authorization/AuthorizeService';
import { USER_LOGIN, USER_LOGOUT } from '../Actions/AuthActionTypes';
import { history } from '../..';
import { ApplicationPaths } from '../../components/api-authorization/ApiAuthorizationConstants';

const initialState = {
    isAuthenticated: false,
    userId: null
}

const authReducer = function (state = initialState, action) {

    if (action.error) {
        if (action.error.response && action.error.response.status === 401) {
            history.push({ pathname: `${ApplicationPaths.LogOut}`, state: { local: true } });
        }
    }

    switch (action.type) {

        case USER_LOGIN:
            return update(state, {
                isAuthenticated: { $set: action.isAuthenticated },
                userId: { $set: action.user.sub }
            });

        case USER_LOGOUT:
            return update(state, {
                isAuthenticated: { $set: false },
                userId: { $set: null }
            });

        default:
            return state;
    }
};

export default authReducer;