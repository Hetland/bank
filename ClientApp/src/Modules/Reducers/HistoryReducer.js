﻿import {
    POST_HISTORY, POST_HISTORY_SUCCESS, POST_HISTORY_ERROR,
    DELETE_HISTORY,
    PUT_HISTORY,
    GET_HISTORY, GET_HISTORY_SUCCESS, GET_HISTORY_ERROR
} from "../Actions/HistoryActionTypes";
import * as _ from 'lodash';
import * as update from 'immutability-helper';
import { concatItemsWithId } from "../../Utils/HelperFunctions";

const initialState = {
    historys: [],
    isLoading: false,
    isCreatingNewHistory: false,
    error: null
}

const historyReducer = function (state = initialState, action) {
    switch (action.type) {

        case GET_HISTORY:
            return { ...state, isLoading: true }

        case GET_HISTORY_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                historys: { $apply: (historys) => concatItemsWithId(historys, [action.value]) }
            });

        case GET_HISTORY_ERROR:
            return { ...state, isLoading: false, error: action.error }



        case POST_HISTORY:
            return { ...state, isCreatingNewHistory: true };

        case POST_HISTORY_SUCCESS:
            const newHistorys = _.cloneDeep(state.historys);
            newHistorys.unshift(action.value);
            return { ...state, historys: newHistorys, isCreatingNewHistory: false }

        case POST_HISTORY_ERROR:
            return { ...state, isCreatingNewHistory: false, error: action.error }



        case DELETE_HISTORY:
            throw Error('deleteHistory is not implemented!');

        case PUT_HISTORY:
            throw Error('putHistory is not implemented!');

        default:
            return state;
    }
};

export default historyReducer;
