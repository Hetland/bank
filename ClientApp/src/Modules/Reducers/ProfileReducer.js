﻿import {
    POST_PROFILE, POST_PROFILE_SUCCESS, POST_PROFILE_ERROR,
    DELETE_PROFILE,
    PUT_PROFILE,
    GET_PROFILES, GET_PROFILES_SUCCESS, GET_PROFILES_ERROR,
    GET_PROFILE, GET_PROFILE_SUCCESS, GET_PROFILE_ERROR,
} from "../Actions/ProfileActionTypes";
import * as _ from 'lodash';
import * as update from 'immutability-helper';
import { concatItemsWithId } from "../../Utils/HelperFunctions";

const initialState = {
    profiles: [],
    isLoading: false,
    isCreatingNewProfile: false,
    error: null
}

const profileReducer = function (state = initialState, action) {
    switch (action.type) {
        case GET_PROFILES:
            return { ...state, isLoading: true }

        case GET_PROFILES_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                profiles: { $apply: (profiles) => concatItemsWithId(profiles, action.value) }
            });

        case GET_PROFILES_ERROR:
            return { ...state, isLoading: false, error: action.error }




        case GET_PROFILE:
            return { ...state, isLoading: true }

        case GET_PROFILE_SUCCESS:
            return update(state, {
                isLoading: { $set: false },
                profiles: { $apply: (profiles) => concatItemsWithId(profiles, [action.value]) }
            });

        case GET_PROFILE_ERROR:
            return { ...state, isLoading: false, error: action.error }



        case POST_PROFILE:
            return { ...state, isCreatingNewProfile: true };

        case POST_PROFILE_SUCCESS:
            const newProfiles = _.cloneDeep(state.profiles);
            newProfiles.unshift(action.value);
            return { ...state, profiles: newProfiles, isCreatingNewProfile: false }

        case POST_PROFILE_ERROR:
            return { ...state, isCreatingNewProfile: false, error: action.error }



        case DELETE_PROFILE:
            throw Error('deleteProfile is not implemented!');

        case PUT_PROFILE:
            throw Error('putProfile is not implemented!');

        default:
            return state;
    }
};

export default profileReducer;
