﻿export const bankaccountsSelector = ({ bankaccountState, authState }) => {
    return {
        bankaccounts: bankaccountState.bankaccounts,
        isLoading: bankaccountState.isLoading,
        error: bankaccountState.error,
        userId: authState.userId
    };
}

export const bankaccountSelector = ({ bankaccountState, authState }, props) => {
    const bankaccount = bankaccountState.bankaccounts.find(g => g.id === props.id);
    const userId = authState.userId;
    const isOwner = bankaccount && bankaccount.profileId && (bankaccount.profileId === userId);
    return {
        bankaccount,
        userId,
        isOwner
    };
}
