﻿export const accounttypesSelector = ({ accounttypeState}) => {
    return {
        accounttypes: accounttypeState.accounttypes,
        isLoading: accounttypeState.isLoading,
        error: accounttypeState.error,
    };
}

export const accounttypeSelector = ({ accounttypeState}, props) => {
    const accounttype = accounttypeState.accounttypes.find(at => at.id === props.id);
    return {
        accounttype,
    };
}
