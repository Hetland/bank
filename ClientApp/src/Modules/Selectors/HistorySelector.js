﻿export const historysSelector = ({ historyState, authState }) => {
    return {
        historys: historyState.historys,
        isLoading: historyState.isLoading,
        error: historyState.error,
        userId: authState.userId
    };
}

export const historySelector = ({ historyState}, props) => {
    const history = historyState.historys.find(g => g.id === props.id);
    return {
        history,
    };
}
