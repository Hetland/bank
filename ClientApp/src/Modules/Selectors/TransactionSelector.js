﻿export const transactionsSelector = ({ transactionState, authState }) => {
    return {
        transactions: transactionState.transactions,
        isLoading: transactionState.isLoading,
        error: transactionState.error,
        userId: authState.userId
    };
}

export const transactionSelector = ({ transactionState}, props) => {
    const transaction = transactionState.transactions.find(g => g.id === props.id);
    return {
        transaction
    };
}
