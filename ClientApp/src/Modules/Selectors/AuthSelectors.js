﻿export const authSelector = ({ authState }) => {
    return {
        isAuthenticated: authState.isAuthenticated,
        userId: authState.userId
    };
}

