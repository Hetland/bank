﻿using Bank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bank
{
    public static class SeedData
    {
        static Random rnd = new Random();

        // For profiles
        static string[] firstNames = { "James T.", "Amelia", "Ember" };
        static string[] lastNames = { "Kirk", "Earhart", "Flocksworth" };
        static string[] emails = { "future@email.com", "past@email.com", "imaginary@email.com" };
        static int[] ages = { 20, 42, 120 };
        static string[] phonenumbers = { "31491840", "20967649", "47068573" };
        //static DateTime[] dateOfBirths = { new DateTime(2320, 1, 20), new DateTime(1925, 5, 15), new DateTime(1992, 4, 30) };

        // For account types
        static string[] accountTypeNames = { "Regular", "Loyal", "Savings" };
        static float[] rents = { 1.2f, 2.0f, 1.8f };

        // For bank accounts
        static int[] internationalAccountIdentifier = { rnd.Next(100000000, 999999999), rnd.Next(100000000, 999999999), rnd.Next(100000000, 999999999) };
        static float[] moneyInAccounts = { 1000.0f, 10000.0f, 100000.0f };

        // For transactions
        static bool[] incomings = { true, true, true, true, false, false, false, true, false };
        static float[] amounts = { 100.0f, 3568.2f, 2569.98f, 140979.1f, 194837.3f, 83987.8f, 133458.4f, 425979.5f, 428058.16f };

        static ApplicationUser CreateUser(int num)
        {
            return new ApplicationUser
            {
                UserName = (firstNames[num] + lastNames[num]),
                PasswordHash = firstNames[num],
                Email = emails[num],
            };
        }

        static Profile CreateProfile(int num, string id)
        {
            return new Profile
            {
                UserId = id,
                Email = emails[num],
                PhoneNumber = phonenumbers[num],
                FirstName = firstNames[num],
                LastName = lastNames[num]
            };
        }

        static AccountType CreateAccountTypes(int typenum)
        {
            AccountType type = new AccountType
            {
                Name = accountTypeNames[typenum],
                Rent = rents[typenum]
            };
            return type;
        }

        public static BankAccount[] CreateBankAccounts()
        {
            BankAccount[] accounts = new BankAccount[3];
            for (int bankaccountnum = 0; bankaccountnum < 3; bankaccountnum++)
            {
                accounts[bankaccountnum] = new BankAccount
                {
                    InternationalAccountIdentifier = internationalAccountIdentifier[bankaccountnum],
                    MoneyInAccount = moneyInAccounts[bankaccountnum],
                    AccountType = CreateAccountTypes(bankaccountnum),
                    History = CreateHistory(bankaccountnum)
                };
            };
            return accounts;
        }

        static Transaction[] CreateTransactions(int bankaccountnum)
        {
            Transaction[] transactions = new Transaction[3];
            for (int transactionnum = 0; transactionnum < 3; transactionnum++)
            {
                transactions[transactionnum] = new Transaction
                {
                    Incoming = incomings[transactionnum + 3 * bankaccountnum],
                    Amount = amounts[transactionnum + 3 * bankaccountnum],
                    TimeOfTransaction = new DateTime(rnd.Next(2012, 2020), rnd.Next(1, 10), rnd.Next(1, 29)),
                    INTLAccountNumber = rnd.Next(100000000, 999999999),
                };
            }
            return transactions;
        }

        static History CreateHistory(int historynum)
        {
            History history = new History
            {
                Transactions = CreateTransactions(historynum)
            };
            return history;
        }

    }
}
