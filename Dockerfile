FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build-env

ARG CONNECTION

# Install NodeJS
RUN curl -sL https://deb.nodesource.com/setup_12.x |  bash -
RUN apt-get -o Acquire::Check-Valid-Until=false install -y nodejs

# Install JSON CLI
RUN apt-get -y install jq

# Create the solution
WORKDIR /source
WORKDIR /Bank
COPY ["./Bank.csproj", "./"]
RUN dotnet restore "Bank.csproj"
COPY . .
WORKDIR ../
RUN dotnet new sln --name Bank
RUN dotnet sln Bank.sln add ./Bank/Bank.csproj
RUN dotnet publish -c Release  -o /publish 


FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim
WORKDIR /publish
COPY --from=build-env /publish .
ENTRYPOINT ["dotnet", "Bank.dll"]